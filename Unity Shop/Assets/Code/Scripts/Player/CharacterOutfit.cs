using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterOutfit : MonoBehaviour
{

    Animator animator;
    public int id;

    void Start()
    {
        animator = GetComponent<Animator>();
        animator.speed = 0.2f;
    }

    public void SetupAnimator(Vector2 playerDir)
    {
        if (!animator) animator = GetComponent<Animator>();
        animator.SetFloat("Horizontal", playerDir.x);
        animator.SetFloat("Vertical", playerDir.y);
    }
}
