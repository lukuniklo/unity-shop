using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{

    [SerializeField]
    private InventoryUI ui;


    [SerializeField]
    private Inventory inventory;

    [SerializeField]
    private Transform outfitParent;

    public List<Item> initialItems = new List<Item>();

    void Start()
    {
        SetupUI();
    }

    void SetupUI()
    {
        ui.SetupUI(inventory.Size);
        ui.OnSelectItem += HandleItemSelection;
        ui.OnEquipItem += HandleItemEquip;
        inventory.Setup();

        foreach (Item item in initialItems)
        {
            if (item.IsEmpty)
                continue;
            inventory.Add(item);
        }
    }

    private void HandleItemSelection(int itemIndex)
    {
        InventoryItem inventoryItem = inventory.GetItemAt(itemIndex);
        if (inventoryItem.IsEmpty) return;
        Item item = inventoryItem.item;
        ui.UpdateDescription(itemIndex, item.Icon, item.Name, item.Description, item.Price.ToString());
    }

    private void HandleItemEquip(int itemIndex)
    {
        InventoryItem inventoryItem = inventory.GetItemAt(itemIndex);
        if (inventoryItem.IsEmpty)
        {
            Debug.Log("EMPTY");
            return;
        }
        Item item = inventoryItem.item;

        if (item.isEquipped)
        {
            // Remove equipement
            foreach (Transform child in outfitParent)
            {
                if (child.GetComponent<CharacterOutfit>().id == item.ID)
                {
                    Destroy(child.gameObject);
                }
            }
        }
        else
        {
            // Add equipement
            CharacterOutfit outfit = Instantiate(item.Outfit, transform.position, Quaternion.identity);
            outfit.transform.SetParent(outfitParent);
            outfit.id = item.ID;
            outfit.SetupAnimator(GetComponent<PlayerController>().moveDir);
        }

        item.isEquipped = !item.isEquipped;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && ui.isActiveAndEnabled)
        {
            ui.Close();
            Global.canPlayerMove = true;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (ui.isActiveAndEnabled)
            {
                ui.Close();
                Global.canPlayerMove = true;
            }
            else if (Global.canPlayerMove)
            {
                Global.canPlayerMove = false;
                ui.Open();

                foreach (var item in inventory.GetItems())
                {
                    ui.UpdateData(item.Key, item.Value.item.Icon);
                }
            }
        }
    }
}
