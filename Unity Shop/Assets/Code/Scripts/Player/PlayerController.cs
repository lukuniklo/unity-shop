using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    public LayerMask collisionMask;
    private float moveSpeed = 4f;
    private Animator animator;
    private float keepMovementThreshold = 0.05f;
    Vector2 movement;
    private GameObject interaction;
    private GameObject clothes;
    public Vector2 moveDir;
    public Transform movePoint;

    void Start()
    {
        movePoint.parent = null;
        animator = this.transform.Find("Sprite").GetComponent<Animator>();
        clothes = this.transform.Find("Clothes").gameObject;
        animator.speed = 0.2f;
    }

    void Update()
    {
        if (Global.canPlayerMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);

            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
        }
        // Reset all movement so player doesn't move by itself after closing a menu
        else if (movement.x != 0 || movement.y != 0)
        {
            movement.x = 0;
            movement.y = 0;
            animator.SetFloat("Speed", 0);
            movePoint.position = transform.position;
        }

        if (movement.x != 0 || movement.y != 0)
        {
            moveDir = movement;
            animator.SetFloat("Horizontal", movement.x);
            animator.SetFloat("Vertical", movement.y);
            animator.SetFloat("Speed", 1);

            // Apply blend tree values for each outfit piece
            foreach (Transform child in clothes.transform)
            {
                child.gameObject.GetComponent<Animator>().SetFloat("Horizontal", movement.x);
                child.gameObject.GetComponent<Animator>().SetFloat("Vertical", movement.y);
                child.gameObject.GetComponent<Animator>().SetFloat("Speed", 1);
            }
        }
        else
        {
            animator.SetFloat("Speed", 0);

            foreach (Transform child in clothes.transform)
            {
                child.gameObject.GetComponent<Animator>().SetFloat("Speed", 0);
            }
        }

        if (Vector2.Distance(transform.position, movePoint.position) <= keepMovementThreshold)
        {
            Vector3 targetX = new Vector3(movement.x, 0f, 0f);
            Vector3 targetY = new Vector3(0f, movement.y, 0f);

            if (Mathf.Abs(movement.x) == 1f && !Physics2D.OverlapCircle(movePoint.position + targetX, 0.2f, collisionMask))
            {
                movePoint.position += targetX;
            }

            if (Mathf.Abs(movement.y) == 1f && !Physics2D.OverlapCircle(movePoint.position + targetY, 0.2f, collisionMask))
            {
                movePoint.position += targetY;
            }
        }

        if (interaction != null && Input.GetKeyDown(KeyCode.Space) && Global.canPlayerMove)
        {
            // Could be replaced by a base class "Interactable" instead. Decided to do not do so to keep it simple.
            interaction.GetComponent<ShopController>().TriggerInteraction();
            Global.canPlayerMove = false;
        }

    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "NPC")
        {
            interaction = col.gameObject;
            interaction.GetComponent<ShopController>().ShowText();
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject == interaction)
        {
            interaction.GetComponent<ShopController>().HideText();
            interaction = null;
        }
    }

    // Guarantee that if we sold an equipped item it's removed as well
    public void RemoveItem(int id)
    {
        foreach (Transform child in clothes.transform)
        {
            if (child.gameObject.GetComponent<CharacterOutfit>().id == id)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
