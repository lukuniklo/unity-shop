using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    [SerializeField]
    private InventorySlot slotPrefab;
    [SerializeField]
    private InventoryDescription itemDescription;
    [SerializeField]
    private RectTransform contentPanel;

    List<InventorySlot> items = new List<InventorySlot>();
    int prevItemIndex = -1;

    public event Action<int> OnSelectItem, OnEquipItem;

    private void Awake()
    {
        Close();
        itemDescription.Clear();
    }

    public void SetupUI(int inventorySize)
    {
        for (int i = 0; i < inventorySize; i++)
        {
            InventorySlot slot = Instantiate(slotPrefab, Vector3.zero, Quaternion.identity);
            slot.transform.SetParent(contentPanel);
            slot.OnItemClicked += HandleSelectedItem;
            slot.OnItemRightClicked += HandleEquippedItem;
            items.Add(slot);
        }
    }

    public void ClearDescription()
    {
        itemDescription.Clear();
    }

    public void HandleEquippedItem(InventorySlot slot)
    {
        int index = items.IndexOf(slot);
        // Doesn't exist
        if (index == -1) return;
        OnEquipItem?.Invoke(index);
    }

    public void HandleSelectedItem(InventorySlot slot)
    {
        int index = items.IndexOf(slot);
        // Doesn't exist
        if (index == -1) return;
        OnSelectItem?.Invoke(index);
    }

    public void UpdateData(int slotIndex, Sprite icon)
    {
        if (items.Count > slotIndex)
        {
            items[slotIndex].SetData(icon);
        }
    }

    public void UpdateDescription(int index, Sprite icon, string name, string description, string price)
    {
        itemDescription.SetDescription(icon, name, description, price);

        if (prevItemIndex != -1)
        {
            items[prevItemIndex].Deselect();
        }

        items[index].Select();
        prevItemIndex = index;
    }

    public void Open()
    {
        itemDescription.Clear();
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);

        foreach (InventorySlot slot in items)
        {
            slot.Deselect();
        }
    }
}
