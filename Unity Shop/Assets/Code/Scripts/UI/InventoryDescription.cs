using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDescription : MonoBehaviour
{

    [SerializeField]
    protected Image itemIcon;
    [SerializeField]
    protected TMP_Text title;
    [SerializeField]
    protected TMP_Text description;

    public virtual void Clear()
    {
        itemIcon.gameObject.SetActive(false);
        title.text = "";

        if (description)
        {

            description.text = "";
        }

    }

    public virtual void SetDescription(Sprite icon, string name, string itemDescription = "", string itemPrice = "")
    {
        itemIcon.gameObject.SetActive(true);
        itemIcon.sprite = icon;
        title.text = name;

        if (description)
        {
            description.text = itemDescription;
        }

    }
}
