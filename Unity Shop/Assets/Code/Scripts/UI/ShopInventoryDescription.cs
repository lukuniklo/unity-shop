using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopInventoryDescription : InventoryDescription
{
    [SerializeField]
    private TMP_Text price;
    [SerializeField]
    private bool isSelling;
    override public void Clear()
    {
        itemIcon.gameObject.SetActive(false);
        title.text = "";

        if (description)
        {

            description.text = "";
        }

        if (price)
        {
            price.text = isSelling ? "Sell" : "Buy";
        }
    }

    override public void SetDescription(Sprite icon, string name, string itemDescription = "", string itemPrice = "")
    {
        int priceNumber = Int32.Parse(itemPrice);
        itemIcon.gameObject.SetActive(true);
        itemIcon.sprite = icon;
        title.text = name;

        if (description)
        {
            description.text = itemDescription;
        }

        if (price)
        {
            price.text = isSelling ? String.Format("Sell ($ {0})", itemPrice) : String.Format("Buy ($ {0})", itemPrice);
        }
    }
}

