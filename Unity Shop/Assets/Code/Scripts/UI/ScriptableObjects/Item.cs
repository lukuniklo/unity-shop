using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class Item : ScriptableObject
{

    public int ID => GetInstanceID();

    [field: SerializeField]
    public string Name { get; set; }
    [field: SerializeField]
    [field: TextArea]
    public string Description { get; set; }
    [field: SerializeField]
    public Sprite Icon { get; set; }
    [field: SerializeField]
    public int Price { get; set; }
    [field: SerializeField]
    public CharacterOutfit Outfit;
    [field: SerializeField]
    public bool IsEmpty { get; set; }

    public bool isEquipped = false;
}

