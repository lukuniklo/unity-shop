using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Inventory : ScriptableObject
{
    [SerializeField]
    private List<InventoryItem> items;
    [field: SerializeField]
    public int Size { get; private set; } = 9;

    public void Setup()
    {
        items = new List<InventoryItem>();

        for (int i = items.Count; i < Size; i++)
        {
            items.Add(InventoryItem.GetEmpty());
        }
    }

    public void Add(Item item)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].IsEmpty)
            {
                items[i] = new InventoryItem
                {
                    item = item
                };
                return;
            }
        }
    }

    public void RemoveAt(int index)
    {
        items[index] = InventoryItem.GetEmpty();
    }

    public InventoryItem GetItemAt(int index)
    {
        return items[index];
    }

    public Dictionary<int, InventoryItem> GetItems()
    {
        Dictionary<int, InventoryItem> result = new Dictionary<int, InventoryItem>();

        for (int i = 0; i < items.Count; i++)
        {
            // Ignore empty slots
            if (items[i].IsEmpty) continue;
            result[i] = items[i];
        }

        return result;
    }
}

[Serializable]
public struct InventoryItem
{
    public Item item;
    public bool IsEmpty => item == null;

    public static InventoryItem GetEmpty() => new InventoryItem { item = null };
}
