using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Image icon;
    [SerializeField]
    private Image selectionIndicator;

    public event Action<InventorySlot> OnItemClicked, OnItemRightClicked;

    private bool empty = true;
    private bool isEquipped = false;

    public void Awake()
    {
        Clear();
        Deselect();
    }

    public void Clear()
    {
        icon.gameObject.SetActive(false);
        empty = true;

    }

    public void Deselect()
    {
        selectionIndicator.enabled = false;
    }

    public void Select()
    {
        selectionIndicator.enabled = true;
    }

    public void SetData(Sprite itemIcon)
    {
        icon.gameObject.SetActive(true);
        icon.sprite = itemIcon;
    }

    public void OnPointerClick(PointerEventData pd)
    {
        if (pd.button == PointerEventData.InputButton.Right)
        {
            OnItemRightClicked?.Invoke(this);
            isEquipped = !isEquipped;
        }
        else
        {
            OnItemClicked?.Invoke(this);
        }
    }
}
