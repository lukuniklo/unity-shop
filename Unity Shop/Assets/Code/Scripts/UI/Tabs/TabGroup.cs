using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    [SerializeField]
    public GameObject tabPagesContainer;
    public List<TabButton> tabs;

    TabButton selectedTab;

    public void Subscribe(TabButton button)
    {
        if (tabs == null)
        {
            tabs = new List<TabButton>();
        }

        tabs.Add(button);
    }

    public void OnTabEnter(TabButton button)
    {
        ClearTabs();

        if (button != selectedTab)
        {
            var tmpColor = button.GetComponent<Image>().color;
            tmpColor.a = 0.8f;
            button.GetComponent<Image>().color = tmpColor;
        }
    }

    public void OnTabExit(TabButton button)
    {
        ClearTabs();

        if (button != selectedTab)
        {
            var tmpColor = button.GetComponent<Image>().color;
            tmpColor.a = 0.6f;
            button.GetComponent<Image>().color = tmpColor;
        }
    }

    public void OnTabSelected(TabButton button)
    {
        var tmpColor = button.GetComponent<Image>().color;
        if (selectedTab)
        {
            tmpColor.a = 0.6f;
            selectedTab.GetComponent<Image>().color = tmpColor;
        }

        ClearTabs();
        selectedTab = button;
        tmpColor = button.GetComponent<Image>().color;
        tmpColor.a = 1f;
        selectedTab.GetComponent<Image>().color = tmpColor;

        int index = button.transform.GetSiblingIndex();

        for (int i = 0; i < tabPagesContainer.transform.childCount; i++)
        {
            if (i == index)
            {
                tabPagesContainer.transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                tabPagesContainer.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void ClearTabs()
    {
        foreach (TabButton button in tabs)
        {
            if (selectedTab != null && button == selectedTab) { continue; }
        }
    }

}
