using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    public List<Item> itemsForSale = new List<Item>();
    [SerializeField]
    private GameObject ui;
    [SerializeField]
    private InventoryUI uiBuy;
    [SerializeField]
    private InventoryUI uiSell;
    [SerializeField]
    private Inventory buyInventory;
    [SerializeField]
    private Inventory sellInventory;
    [SerializeField]
    private PlayerController player;


    [SerializeField]
    private Button buttonBuy;
    [SerializeField]
    private Button buttonSell;
    [SerializeField]
    private TextMesh text;

    Item selectedItem;
    int selectedItemIndex;


    void Start()
    {
        SetupUI();
    }

    void SetupUI()
    {
        text.gameObject.SetActive(false);
        ui.SetActive(false);
        uiBuy.SetupUI(buyInventory.Size);
        uiBuy.OnSelectItem += HandleBuyItemSelection;

        uiSell.SetupUI(sellInventory.Size);
        uiSell.OnSelectItem += HandleSellItemSelection;

        buttonBuy.onClick.AddListener(OnBuyItem);
        buttonSell.onClick.AddListener(OnSellItem);

        buyInventory.Setup();

        foreach (Item item in itemsForSale)
        {
            if (item.IsEmpty)
                continue;
            buyInventory.Add(item);
        }

        foreach (var item in buyInventory.GetItems())
        {
            uiBuy.UpdateData(item.Key, item.Value.item.Icon);
        }

        foreach (var item in sellInventory.GetItems())
        {
            uiSell.UpdateData(item.Key, item.Value.item.Icon);
        }
    }

    private void HandleBuyItemSelection(int itemIndex)
    {
        InventoryItem inventoryItem = buyInventory.GetItemAt(itemIndex);
        if (inventoryItem.IsEmpty) return;
        Item item = inventoryItem.item;
        uiBuy.UpdateDescription(itemIndex, item.Icon, item.Name, item.Description, item.Price.ToString());
        selectedItemIndex = itemIndex;
        selectedItem = item;

        if (item.Price > Global.gold)
        {
            buttonBuy.interactable = false;
        }
        else
        {
            buttonBuy.interactable = true;
        }
    }

    private void HandleSellItemSelection(int itemIndex)
    {
        InventoryItem inventoryItem = sellInventory.GetItemAt(itemIndex);
        if (inventoryItem.IsEmpty) return;
        Item item = inventoryItem.item;
        uiSell.UpdateDescription(itemIndex, item.Icon, item.Name, item.Description, item.Price.ToString());
        selectedItemIndex = itemIndex;
        selectedItem = item;
        buttonSell.interactable = true;
    }

    void OnBuyItem()
    {
        Global.gold -= selectedItem.Price;
        sellInventory.Add(selectedItem);

        foreach (var item in sellInventory.GetItems())
        {
            uiSell.UpdateData(item.Key, item.Value.item.Icon);
        }

        if (selectedItem.Price > Global.gold)
        {
            buttonBuy.interactable = false;
        }
    }

    void OnSellItem()
    {
        player.RemoveItem(selectedItem.ID);
        Global.gold += selectedItem.Price;
        sellInventory.RemoveAt(selectedItemIndex);
        buttonSell.interactable = false;
        // If we decide items can be stacked, this won't work because we may have remaining items!
        uiSell.UpdateData(selectedItemIndex, null);
        uiSell.ClearDescription();
    }


    public void TriggerInteraction()
    {
        if (!Global.canPlayerMove) return;

        ui.SetActive(true);
        uiBuy.Open();
        foreach (var item in buyInventory.GetItems())
        {
            uiBuy.UpdateData(item.Key, item.Value.item.Icon);
        }

        foreach (var item in sellInventory.GetItems())
        {
            uiSell.UpdateData(item.Key, item.Value.item.Icon);
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && ui.gameObject.active)
        {
            buttonBuy.interactable = false;
            ui.SetActive(false);
            uiBuy.Close();
            uiSell.Close();
            selectedItem = null;
            selectedItemIndex = -1;
            Global.canPlayerMove = true;
        }
    }

    public void ShowText()
    {
        text.gameObject.SetActive(true);
    }

    public void HideText()
    {
        text.gameObject.SetActive(false);
    }
}
