![Jan-06-2024_16-56-43](/uploads/6f67ce258b67d8805f3b6c7de76df5b1/Jan-06-2024_16-56-43.mp4)

# Unity Outfit Shop

- [HTML5 Live Demo](https://cuca.itch.io/unity-shop?password=312)
In the repo there's also a macOS build, but I recommend the HTML5 one since I optimized the UI for resolution there.

The goal of the project was to create a Shoopkeeper where the player can buy and sell items. The items purchased should be equipable and appear as the player outfit.

# Systems

My approach for the outfit was using the same configuration for each Animator & animations, which may not be the best option for scaling, where I would probably instead make animations by code (supposing that as in this project, all the clothing animation followed the same sheet frames order), making it easier and faster to add new clothes.

The inventory was made in a modular way so both the NPC and playe can reuse most of the same contents and since items are all Scriptable Objects, designers could easily create and balance from engine menus without any trouble or messing with code themselves. A Slot prefab is responsible for rendering its contents, managed by `InventoryUI.cs`. Then a "model" layer (`InventoryController.cs` for the Player and `ShopController.cs` for the shop) come in to orchestrate everything (when to open or close), re-render updated contents

Due to some problems I faced (mostly crashes, maybe due the fact I'm using this version on a Mac M2?) I ended up having some game objects corrupted and had to recreate all animations and animators from scratch, so I ended up using only two items as a core to show the system working without compromising the remaining time.

# Instructions

- WASD to move
- <kbd>E</kbd> to toggle inventory (also has <kbd>ESC</kbd> for closing it)
- <kbd>Spacebar</kbd> to interact with NPC
- <kbd>ESC</kbd> to close the shop
- LMB to select an inventory item
- RMB to equip selected item

# Personal assessment

I tried to make everything in a modular way, which could be both expanded and used in the future, without being too opinated. I made a big usage of `[SerializeField]` in order to avoid calling `Find` the whole time and also thinking about a game designer who might want to explore and rearrange things by themselves.

I think the UI could be prettier, I tried to make a nice UX at least and in a real project and given more time, would add some tweens and motion for things, such as the NPC interaction text popping up, some dialogue before opening the shop with the options to trade or dismiss it, etc.

Given the requirements, though, I feel I was able to achieve the expected result, although I missed also adding the UI for the player Gold (I was pushing it through development and then got out of time to add it), which would be a small but cool addition to have.

I also choose to not add anything further the one-screen shown since I didn't wanted to risk by adding a camera breaking something and lose time due it.

One thing that also got in my way is that while setting up the LFS, some files got corrupted and I couldn't recover them (only the scripts), so I had to re-do some stuff. But I tried my best to keep the code with quality.